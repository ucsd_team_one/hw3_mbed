#ifndef VOLTMETER_H
#define VOLTMETER_H

#include "mbed.h"

Serial pc(USBTX, USBRX);
AnalogIn mypot(p20);

const float DELAY = 0.5;            // in seconds
const float VOLTAGE_FACTOR = 3.3;   // in volts

const char cls[] = "\x1B[2J";     // VT100 erase screen
const char home[] = "\x1B[H";     // VT100 home

// convert measurement m to voltage
float voltage(const float m);

#endif
