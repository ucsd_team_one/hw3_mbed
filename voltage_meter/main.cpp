#include "voltmeter.h"

float voltage(const float m) {
    return m * VOLTAGE_FACTOR;
}

int main(void) {
    float i = 0;
    while(true) {
        i = mypot;
        pc.printf(cls);
        pc.printf(home);
        pc.printf("\rMeasurement %4.2f  \n", i);
        pc.printf("\rVoltage %4.2f volt ", voltage(i));
        wait(DELAY);
    }
}
